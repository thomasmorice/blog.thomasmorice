describe('Test all pages', function () {
	
	context('resolution agnostic', function () {
			it ('shows menu state', () => {
				cy.visit('/')
				cy.get('.links a').each(function ($el) {
					cy.visit($el.attr('href'))
					cy.wait(3000)
					cy.get('.menu .text').contains('menu')
					cy.get('.menu').wait(4000).find('.hamburger-icon').click()
					cy.get('.menu .text').contains('close')
					cy.get('.menu').find('.hamburger-icon').click()
					cy.get('.menu .text').contains('menu')
					cy.wait(1000)
				})
			});
	})
	

	
  context('iphone 6+ resolution', function () {
    beforeEach(function () {
			cy.viewport('iphone-6+')
			cy.visit('/')
		})
		
		it ('display my logo', () => {
			cy.get('svg.logo').should('be.visible');
		});

		it('display <h1> in two lines', () => {
			cy.get('h1').should('have.css', 'height', '56px')
		});

		it('should show a featured image', () => {
			cy.get('.featured-content .image-container img').should('be.visible')
		});

		it('contains at least two <p>', () => {
			cy.get('.content').find('p').its('length').should('be.gt', 2)
		})

  })

  context('desktop resolution', function () {
    beforeEach(function () {
      cy.viewport('macbook-11')
		})

		it('should show a featured image', () => {
			cy.get('.featured-content .image-container img').should('be.visible')
		});

		it('should have a featured paragraph', () => {
			cy.get('.featured-content .paragraph').should('be.visible')
		});

		it('should have other paragraphs in the page', () => {
			cy.get('.content:not(.isFeaturedParagraph) p').should('be.visible')
		});

		/*
    it('displays mobile menu on click', function () {
			cy.get('h1').should('have.css', 'height', '86px')
			/*
      cy.get('nav .desktop-menu').should('not.be.visible')
      cy.get('nav .mobile-menu')
        .should('be.visible')
        .find('i.hamburger').click()
			cy.get('ul.slideout-menu').should('be.visible')
			
		})
		*/
  })
})


describe('Sapper template app', () => {
	beforeEach(() => {
		cy.visit('/')
	});
	/*
	it('navigates to /about', () => {
		cy.get('nav a').contains('about').click();
		cy.url().should('include', '/about');
	});

	it('navigates to /blog', () => {
		cy.get('nav a').contains('blog').click();
		cy.url().should('include', '/blog');
	});
	*/
});