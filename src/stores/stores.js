import { writable } from 'svelte/store';

export const currentRoute = writable('/');
export const contentfulPages = writable(false);
export const menuOpened = writable(false);
export const pageScrolled = writable(false);