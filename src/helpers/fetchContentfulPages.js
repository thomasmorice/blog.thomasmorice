import ContentfulPages from '../_models/ContentfulPages'

export async function fetchContentfulPages(fetchWrapper, session) {
  if (!session || !session.contentfulPagesJSON) {  
    const res = await fetchWrapper.fetch(`_all.pages.json`);
    const contentfulPagesJSON = await res.json();
    if (res.status === 200) {
      session.contentfulPagesJSON = contentfulPagesJSON
    } else {
      this.error(res.status, data.message);
    }
  }
  return new ContentfulPages(session.contentfulPagesJSON)
}