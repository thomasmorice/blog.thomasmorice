import * as contentful from 'contentful'

export function getContentfulClient() {
  
  let clientParams = { space: process.env.SPACE_ID};
  if (process.env.MODE === 'production') {
    clientParams.accessToken = process.env.API_TOKEN;
    clientParams.host = process.env.CONTENT_DELIVERY_API
  } else {
    clientParams.accessToken = process.env.API_TOKEN_PREVIEW;
    clientParams.host = process.env.PREVIEW_API
  }
  
  try {
    return contentful.createClient(clientParams)
  } catch (e) {
    console.error('\x1b[31m%s\x1b[0m', 'Seems like the Contentful credentials are not set up properly in the `.env` file.');
    exit()  
  }
}