export default async function isSupportingWebp() {
  if (!self.createImageBitmap) return false;
  
  const webpData = 'data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=';
  const blob = await fetch(webpData).then(r => r.blob());
  return createImageBitmap(blob).then(() => true, () => false);
}

export function getImageWidth() {
  let imgWidth = window.innerWidth - 40;
  if (window.innerWidth > 735) {
    imgWidth = window.innerWidth * 40 / 100 - 100;
  }
  return imgWidth;
}

export async function getImageFormat() {
  let format = 'fm=jpg&fl=progressive';
  if (await isSupportingWebp()) {
      format = 'fm=webp'
  }
  return format;
}