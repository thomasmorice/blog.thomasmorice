import Image from './Image'
import Text from './Text'
import keywordExtractor from 'keyword-extractor'

class ContentfulPage {
  constructor(contentfulPageJSON) {
    this._navigationOrder = contentfulPageJSON.fields.navigationOrder
    this._slug = contentfulPageJSON.fields.slug
    this._metatitle = contentfulPageJSON.fields.metatitle
    this._pageTitle = contentfulPageJSON.fields.pageTitle
    this._featuredImage = new Image(contentfulPageJSON.fields.featuredImage)

    this._content = contentfulPageJSON.fields.content.content.map(richTextElement => {
      switch (richTextElement.nodeType) {
        case 'paragraph': 
          return new Text(richTextElement)
        case 'embedded-asset-block':
          if (richTextElement.data.target.fields.file.contentType.includes('image'))
            return new Image(richTextElement.data.target) 
          else 
            console.log( "\u001b[1;31m A component cannot be rendered : " + richTextElement.data.target.fields.file.contentType);  
        default:
          console.log( "\u001b[1;31m A component cannot be rendered : " + richTextElement.nodeType);
      }
    })
  }
  getNavigationOrder() {
    return this._navigationOrder;
  }
  getSlug() {
    return this._slug;
  }
  getMetatitle() {
    return this._metatitle;
  }
  getTitle() {
    return this._pageTitle;
  }
  getFeaturedImage() {
    return this._featuredImage;
  }
  getMetaImage() {
    return this.getFeaturedImage().getMetaImage();
  }
  getContent() {
    return this._content;
  }
  getFeaturedParagraph() {
    const result = this._content.filter(richTextElement => richTextElement.getType() === 'paragraph');
    return result[0]
  }
  getKeywords() {
    return keywordExtractor.extract(this.getFeaturedParagraph().toString() + ' ' + this.getMetatitle() , {
      language:"english",
      remove_digits: true,
      return_changed_case:true,
      remove_duplicates: true
    });
  }
}

export default ContentfulPage;