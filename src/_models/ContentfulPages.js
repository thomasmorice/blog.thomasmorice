import ContentfulPage from './ContentfulPage'

class ContentfulPages {
  constructor(contentfulPagesJSON) {
    this._contentfulPageList = contentfulPagesJSON.items.map(contentfulPageJSON => {
      return new ContentfulPage(contentfulPageJSON)
    });
  }

  getContentfulPageList() {
    return this._contentfulPageList;
  }

  getPageFromSlug(slug) {
    return this.getContentfulPageList().find(contentfulPage => {
      return contentfulPage.getSlug() === slug;
    });
  }

  getNavigationLinks() {
    return this.getContentfulPageList().map(contentfulPage => {
      return {
        order: contentfulPage.getNavigationOrder(),
        href: contentfulPage.getSlug() === 'index' ? '' : contentfulPage.getSlug() , 
        label: contentfulPage.getTitle()
      }
    }).sort((a, b) => (a.order > b.order) ? 1 : -1);
  }
  getWebsiteUrls() {
    let result = []
    this.getContentfulPageList().forEach(contentfulPage => {
      result.push(contentfulPage.getSlug() === 'index' ? '' : contentfulPage.getSlug().replace(/\/?$/, '/'))
    })
    return result.reverse();
  }
}

export default ContentfulPages