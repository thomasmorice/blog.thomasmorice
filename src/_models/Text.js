import RichTextElement from './RichTextElement'

class Text extends RichTextElement {
  constructor(textJSON) {
    super(textJSON.nodeType);
    this._content = textJSON.content
  }
  getContent() {
    return this._content;
  }
  toString() {
    return this.getContent().map(content => {
      return content.value
    }).join('')
  }
}

export default Text;