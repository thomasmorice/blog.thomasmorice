import RichTextElement from './RichTextElement'

const lowResImageWidth = 20;

class Image extends RichTextElement {
  constructor(featuredImageJSON) {
    super('image')
    this._title = featuredImageJSON.fields.title
    this._url = featuredImageJSON.fields.file.url
    this._description = featuredImageJSON.fields.description
  }
  getHighResImage(width, ratio, format = 'fm=jpg&fl=progressive') {
    return `https:${this._url}?w=${Math.ceil(width  * ratio / 100) * 100}&${format}`
  }
  getUrl() {
    return this._url
  }
  getLowResImage() {
    return `https:${this._url}?w=${lowResImageWidth}&fm=png&fl=png8`
  }
  getMetaImage() {
    return `https:${this._url}?w=1200&h=1200&fit=crop`
  }
  getDescription() {
    return this._description;
  }
}

export default Image; 