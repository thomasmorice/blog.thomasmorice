class RichTextElement {
  constructor(type) {
    this._type = type
  }
  getType() {
    return this._type;
  }
}

export default RichTextElement;