const sendMail = require('sendmail')()
const { validateLength } = require('./utils/validations')

exports.handler = (event, context, callback) => {
  
  const body = JSON.parse(event.body)
  /*
  try {
    validateLength('body.name', body.name, 3, 50)
  }
  catch (e) {
    return callback(null, {
      statusCode: 403,
      body: e.message
    })
  }

  /*
  try {
    validateEmail('body.email', body.email)
  }
  catch (e) {
    return callback(null, {
      statusCode: 403,
      body: e.message
    })
  }
  */

  try {
    validateLength('body.mail.message', body.mail.message, 0, 1000)
  }
  catch (e) {
    return callback(null, {
      statusCode: 403,
      body: JSON.stringify({
        status: 'error',
        message: e.message
      }),
    })
  }

  const descriptor = {
    from: `"visitor@thomasmorice.com" <visitor@thomasmorice.com>`,
    to: `"t.morice4@gmail.com" <t.morice4@gmail.com`,
    subject: `Someone is trying to contact you from the website`,
    text: `Subject : ${body.mail.subject} Message: ${body.mail.message}`,
  }

  if (body.mode === 'debug') {
    callback(null, {
      statusCode: 200,
      body: JSON.stringify({
        status: 'testing',
        descriptor: descriptor
      }),
    })
  } else {
    sendMail(descriptor, (e) => {
      if (e) {
        callback(null, {
          statusCode: 500,
          body: JSON.stringify({
            status: 'error',
            message: e.message
          }),
        })
      }
      else {
        callback(null, {
          statusCode: 200,
          body: JSON.stringify({
            status: 'success'
          }),
        })
      }
    })
  }
}