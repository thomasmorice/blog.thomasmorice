import { getContentfulClient as client } from '../helpers/getContentfulClient'

export function get(req, res, next) {
  client().getEntries({
    'content_type': 'page',
  }).then(function (entries) {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });
    
    res.end(JSON.stringify(entries))
    
  }).catch((error) => {
    console.log(error)
  })
}