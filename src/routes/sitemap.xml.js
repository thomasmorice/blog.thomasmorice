
import { siteUrl } from '../stores/_config.js';
import ContentfulPages from '../_models/ContentfulPages';
import { getContentfulClient as client } from '../helpers/getContentfulClient'

const renderSitemapXml = (contextPaths) => `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
${contextPaths.map(path => `
  <url>
    <loc>${siteUrl}/${path}</loc>
    <changefreq>daily</changefreq>
    <priority>0.7</priority>
  </url>
`).join('\n')}
</urlset>`;

export function get(req, res) {
  client().getEntries({
    'content_type': 'page',
  }).then(function (entries) {
    res.writeHead(200, {
      'Cache-Control': `public, max-age=0, must-revalidate`,
      'Content-Type': 'application/xml'
    });
    
    var contentfulPagesObject = new ContentfulPages(entries)
    const feed = renderSitemapXml(contentfulPagesObject.getWebsiteUrls())
    res.end(feed);
  }).catch((error) => {
    console.log(error)
  })

}