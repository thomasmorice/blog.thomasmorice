<div align="center">

<img src="./static/readme-assets/project-shot.png" style="margin-top: 20px" width="480">

**Simple Portfolio of my website that covers multiple web-development areas**

![Pipeline Status](https://gitlab.com/thomasmorice/thomasmorice-2019/badges/release/master/pipeline.svg?style=flat-square) 
![Website status](https://img.shields.io/website/https/thomasmorice.com.svg?down_message=offline&up_message=alive)
![Uptime Robot ratio (30 days)](https://img.shields.io/uptimerobot/ratio/m780230022-3deb62470445f0944eead88e.svg?style=flat-square)
![Google PageSpeed](https://api.speedbadge.io/v1?url=https://thomasmorice.com&showStratLabel=true&strat=desktop)
![Google PageSpeed](https://api.speedbadge.io/v1?url=https://thomasmorice.com&showStratLabel=true&strat=mobile)
<!--Netlify badge is ugly
[![Netlify Status](https://api.netlify.com/api/v1/badges/e44c9e86-4b1e-4235-8554-54849ed66a56/deploy-status)](https://app.netlify.com/sites/elated-stonebraker-fc0eef/deploys)-->

</div>

## :thinking: About the project
I've decided once again to re-do my website with a fresh new design that I've made under Figma. 
I wanted to learn svelte and sapper, and I wasn't so happy about my previous website for different reasons. I also think having a simple personal project is a great way to learn in different areas around web development. 

This project is meant to move and change through time, I wanna iterate and learn with it.
Here is where this project comes into place. 

## :nerd_face: So what is covered in this project
Here are the different fields that I'm covering while building this website

### Design with Figma
I love spicing my work with some creativity, here everything started with Figma. I've created my design there, simple and minimalist, maybe a bit greyish for some, but it's my taste :man_shrugging:

### Headless CMS with Contentful
<img src="https://images.ctfassets.net/3ouphkrynjol/18vNxOBNqgKA8SsUyYG20w/4a659a380f864cbefd3cbe67f074f609/contentful.png" alt="cypress testing" style="vertical-align: middle; padding-right: 10px;" height="20"/> 
As I mentioned, I'm using sapper for this project, which is an amazing framework for this particular project. I believe that every framework has its purpose, and sapper is great for static site generation. 

I'm using Contentful as a headless CMS, which means every content on this website is actually coming from a content management tool. I'll get deeper into the workflow in [the specific section](##What-is-the-workflow)

### Testing with Cypress
<img src="https://www.cypress.io/static/33498b5f95008093f5f94467c61d20ab/31529/cypress-logo.png" alt="cypress testing" style="vertical-align: middle; padding-right: 10px;" height="20"/> 

It's been quite a while since I want to learn how testing works. This is a good opportunity. I have few tests up and running, but I need to work more on that. 
Some of the tests that I have right now: 

- Every page should have a featured image
- The title of every pages (`<h1>` tag) should fit in two lines
- The pages should contain at least two paragraphs
- The hamburger menu should work and show page links

### Hosting and Lambda function with Netlify
<img src="https://www.netlify.com/img/press/logos/logomark.png" alt="netlify hosting" style="vertical-align: middle; padding-right: 10px;" height="20"/>
Even though I like to build things, I don't know how I can beat Netlify. My site is hosted there, and everytime I push something on gitlab, netlify will create a new environment for this specific version.

The Lambda function is simply taking care of sending mail from my static site.

### Continuous deployment and integration with Gitlab & Netlify
<img src="https://www.gillware.com/wp-content/uploads/2017/02/gitlab-logo-square.png" alt="netlify hosting" style="vertical-align: middle; padding-right: 10px;" height="20"/>
I tend to choose Gitlab, for many reason, mostly because their CI is very solid, I love the interface and their free tier gives you almost all the possibilities.

### SEO with Google Analytics
<img src="https://cdn.www.4me.com/wp-content/uploads/2018/02/google-analytics-logo-300x300.png" alt="google analytics" style="vertical-align: middle; padding-right: 10px;" height="20"/>
Nothing much to say here, I don't know much about analytics in general but I thought it would be interesting to see if people are actually visiting my website.

## :construction_worker: What is the workflow
There are two different ways to change the website: 

1. Changing the code and pushing things to git

I'm pushing code using the git-flow way, and create pull request when I want to deploy to develop or master. Those two branches has automatic test and build in Gitlab and Netlify. Gitlab will keep test artefacts (videos and screenshots) in case test fail.

Every build are deployed automatically in Netlify, unless the cypress test didn't passed (which shouldn't be the case since test are runned when creating pull request in Gitlab).

2. Publishing / unpublishing content in Contentful

When changes happen in Contentful, with the help of a webhook, I trigger a new build on the develop branch in gitlab. I want to keep the production branch safe from changes on the CMS side. Once I'm ok with the changes in develop, I can safely create a pull request in `master`, and run tests before releasing it in production. 

This means that develop should work as a pre-production environment. `master` and `develop` **should definitely** have the same code 99.9% of the time. 

## :heavy_check_mark: What's in the pipeline
- [x] Find a better (nicer) favicon 
- [ ] Smooth scroll on the images
- [ ] Add a footer with contact and link to different stuff
- [ ] Implement test coverage and add the badge to gitlab
- [ ] Test every pages using Cypress
- [ ] Automate the process of bumping version on merges
- [ ] Add some screenshot to this readme file
- [ ] Design a blog front page
- [ ] Add blog pages
- [x] Add page speed automation reports